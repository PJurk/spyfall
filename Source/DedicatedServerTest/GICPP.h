// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "GICPP.generated.h"

/**
 * 
 */
UCLASS()
class DEDICATEDSERVERTEST_API UGICPP : public UGameInstance
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable)
		bool ConnectToServer();

	UFUNCTION(BlueprintCallable)
		bool SendMessage(int32 connectionType, FString message);

	UFUNCTION(BlueprintCallable)
		void StartListening();

	UFUNCTION(BlueprintCallable)
		void EndConnection();

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
		void MessageReceived(const FString& message);

	UPROPERTY(BlueprintReadWrite)
		bool connected;

private:
	bool ClientSend(int32 connectionType, FString msg);
};

class TCPThread : public FNonAbandonableTask
{
public:
	TCPThread();
	TCPThread(bool* connection, UGICPP* gameInstance);
	~TCPThread();

	FORCEINLINE TStatId GetStatId() const
	{
		RETURN_QUICK_DECLARE_CYCLE_STAT(PrimeSearchTask, STATGROUP_ThreadPoolAsyncTasks);
	}

	void DoWork();

private:
	bool* Connection;
	UGICPP* GameInstance;
};