// Fill out your copyright notice in the Description page of Project Settings.

#include "GICPP.h"


#include "Runtime/Networking/Public/Networking.h"
#include "Sockets.h"
#include "SocketSubsystem.h"
#include "Runtime/Engine/Classes/Engine/Engine.h"

FSocket* Socket;

bool UGICPP::ConnectToServer()
{
	Socket = ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM)->CreateSocket(NAME_Stream, TEXT("TCP_Test"), false);

	FIPv4Address ip;
	FIPv4Address::Parse("127.0.0.1", ip);

	TSharedRef<FInternetAddr> addr = ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM)->CreateInternetAddr();
	addr->SetIp(ip.Value);
	addr->SetPort(7777);

	connected = Socket->Connect(*addr);
	return connected;
}

bool UGICPP::SendMessage(int32 connectionType, FString message)
{
	if (connected)
	{
		bool messageSend = ClientSend(connectionType, message);
		if (!messageSend)
			connected = false;
		return messageSend;
	}
	return false;
}


bool UGICPP::ClientSend(int32 connectionType, FString msg)
{
	if (msg.Len() > 0)
	{
		TCHAR* data = msg.GetCharArray().GetData();
		uint8*  dataToSend = (uint8*)TCHAR_TO_UTF8(data);
		int32 size = FCString::Strlen(data);

		TCHAR connection[1] = { connectionType };
		TCHAR dataLength[1] = { size };

		int32 sent = 0;
		bool successful1 = Socket->Send((uint8*)TCHAR_TO_UTF8(connection), 1, sent);

		if (successful1)
		{
			sent = 0;
			bool successful2 = Socket->Send((uint8*)TCHAR_TO_UTF8(dataLength), 1, sent);
			if (successful2)
			{
				sent = 0;
				return Socket->Send(dataToSend, size, sent);
			}
		}
	}
	return false;
}

void UGICPP::StartListening()
{
	FAutoDeleteAsyncTask<TCPThread>* task = new FAutoDeleteAsyncTask<TCPThread>(&connected, this);
	task->StartBackgroundTask();
}

void UGICPP::EndConnection()
{
	connected = false;
	if(Socket!=NULL)
		Socket->Close();
	Socket = NULL;
	UE_LOG(LogTemp, Warning, TEXT("Socket stopped"));
}

///////////////////////////////////////////////////////////////////////
#pragma region Thread
TCPThread::TCPThread()
{
	UE_LOG(LogTemp, Warning, TEXT("Waiting for messagess - default constructor"));
}

TCPThread::TCPThread(bool* connection, UGICPP* gameInstance)
{
	Connection = connection;
	UE_LOG(LogTemp, Warning, TEXT("Waiting for messagess"));
	GameInstance = gameInstance;
}

TCPThread::~TCPThread()
{
	*Connection = false;
	UE_LOG(LogTemp, Warning, TEXT("Finished listening"));
}

void TCPThread::DoWork()
{
	while (*Connection && Socket != NULL)
	{
		uint8* dataLength = new uint8[1];
		int32  read = 0;

		Socket->Recv(dataLength, 1, read);
		if (read != 0)
		{
			int32 messageLength = dataLength[0];
			uint8* data = new uint8[messageLength];

			read = 0;
			bool successful = Socket->Recv(data, dataLength[0], read);
			GameInstance->MessageReceived(((FString)UTF8_TO_TCHAR(data)).Left(messageLength));
		}
	}
	UE_LOG(LogTemp, Warning, TEXT("Stopped listening"));
}
#pragma endregion