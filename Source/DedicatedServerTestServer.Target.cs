// Fill out your copyright notice in the Description page of Project Settings.
using UnrealBuildTool;
using System.Collections.Generic;

public class DedicatedServerTestServerTarget : TargetRules
{
	public DedicatedServerTestServerTarget(TargetInfo Target) : base(Target)
	{
        Type = TargetType.Server;
        ExtraModuleNames.Add("DedicatedServerTest");    // Change this line as shown previously
	}
}