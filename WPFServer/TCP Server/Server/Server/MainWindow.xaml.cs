﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Media;


namespace Server
{

    public class UE4ServerData
    {
        public string Name { get; set; }
        public int Port { get; set; }
        public int Id { get; set; }

        public UE4ServerData(string name, int port, int id)
        {
            Name = name;
            Port = port;
            Id = id;
        }
    }

    public partial class MainWindow : Window
    {
        int i;
        TcpListener server = new TcpListener(IPAddress.Parse("127.0.0.1"), 7777);
        NetworkStream stream;
        TcpClient client;
        Thread startServer;
        Thread listenToStream;
        int NextPort = 7890;
        
        ObservableCollection<UE4ServerData> Proc = new ObservableCollection<UE4ServerData>();

        public MainWindow()
        {
            InitializeComponent();
            DG1.DataContext = Proc;
        }

        private void Button_Click(object sender, EventArgs e)
        {
            StartServer();
        }

        private void Reset_Click(object sender, RoutedEventArgs e)
        {
            server.Stop();
            if (listenToStream != null)
                listenToStream.Abort();
            if (startServer != null)
                startServer.Abort();
            StartServer();
        }
        
        private void StartServer()
        {
            server.Start();
            ServerState.Text = "Server Online";
            ServerState.Foreground = new SolidColorBrush(Color.FromArgb(255, 0, 255, 0));

            
            startServer = new Thread(() =>
            {
                while (true)
                {
                    try
                    {
                        client = server.AcceptTcpClient();
                    }
                    catch (Exception ex)
                    { return; }

                    if (client.Connected)
                        ServerReceive();
                }
            });
            startServer.Start();
        }

        private string generateRoomName()
        {
            Random rand = new Random();
            StringBuilder roomName = new StringBuilder();

            for (int i = 0; i < 6; i++)
            {
                int los = rand.Next(0, 2);
                if (los == 0)
                    roomName.Append((char)rand.Next(48, 58));
                else if (los == 1)
                    roomName.Append((char)rand.Next(65, 91));
            }

            return roomName.ToString();
        }

        public void ServerReceive()
        {
            byte[] connectionType = new byte[1];
            byte[] datalength = new byte[1];
            stream = client.GetStream();
            listenToStream = new Thread(() =>
            {
                stream.Read(connectionType, 0, 1);
                stream.Read(datalength, 0, 1);
                byte[] data = new byte[datalength[0]];
                stream.Read(data, 0, data.Length);
                Dispatcher.Invoke(() => 
                {
                    string message = Encoding.Default.GetString(data);
                    string roomName = generateRoomName();
                    UE4ServerData[] rooms = Proc.Select(n => n).Where(n => n.Name == roomName).ToArray();
                    if (connectionType[0] == 0) //0 - create new room, 1 - connect to room
                    {
                        if (!rooms.Any())
                            ServerSend(OpenNewServer(message, roomName));
                        else
                            ServerSend("Change room name");
                    }
                    else if (connectionType[0] == 1)
                    {
                        rooms = Proc.Select(n => n).Where(n => n.Name == message).ToArray();
                        if (rooms.Any())
                            ServerSend(rooms[0].Port.ToString());
                        else
                            ServerSend("Change room name");
                    }
                    else
                    {
                        rooms = Proc.Select(n => n).Where(n => n.Name == message).ToArray();
                        if (rooms.Any())
                            DestroyServer(message);
                        else
                            ServerSend("Unable to close server");
                    }
                });
            });
            listenToStream.Start();
        }
        
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            StopAllServers();
            server.Stop();
            if (listenToStream != null)
                listenToStream.Abort();
            if (startServer != null)
                startServer.Abort();
        }

        public void ServerSend(string msg)
        {
            stream = client.GetStream();
            byte[] data = Encoding.Default.GetBytes(msg);
            int length = data.Length;
            byte[] datalength = new byte[1];
            datalength = BitConverter.GetBytes(length);
            stream.Write(datalength, 0, 1);
            stream.Write(data, 0, data.Length);
        }

        // Co musi dostać:
        // Maksymalna ilosc graczy       
        // Dodatkowe tryby (wiecej szpiegow itp.)
        // Poziom trudnosci dla szpiega
        // Czy jest limit czasowy, jesli jest to limit czasowy

        private string OpenNewServer(string message, string roomName)
        {
            string[] tab = message.Split('?');
            Debug.WriteLine(tab);

            try
            {
                Process myProcess = new Process();
                myProcess.StartInfo.UseShellExecute = false;
                string actualPorty = NextPort.ToString();
                myProcess.StartInfo.FileName = @"D:\Servers_Client\ServerTest\DedicatedServerTest\Windows\WindowsNoEditor\DedicatedServerTest\Binaries\Win64\DedicatedServerTestServer.exe";
                myProcess.StartInfo.Arguments = "?NazwaGry=" + roomName + "?MaxGraczy=" + tab[0] + "?WiecejSzpiegow=" + tab[1] + "?Trudnosc=" + tab[2] + "?CzyLimit=" + tab[3] + "?Limit=" + tab[4] +" -log -port=" + actualPorty;
                myProcess.EnableRaisingEvents = true;
                myProcess.Exited += new EventHandler(RemoveServerFromList);
                myProcess.Start();
                Proc.Add(new UE4ServerData(roomName, NextPort, myProcess.Id));
                NextPort++;
                return actualPorty;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }
        
        private void StopServer_Click(object sender, RoutedEventArgs e)
        {
            StopAllServers();
        }

        private void StopAllServers()
        {
            try
            {
                foreach (var UE4Proc in Proc)
                {
                    Process toKill = Process.GetProcessById(UE4Proc.Id);
                    toKill.Kill();
                }
                Proc.Clear();
            }
            catch (Exception ex)
            {
            }
        }

        private void RemoveServerFromList(object sender, EventArgs e)
        {
            int procId = (sender as Process).Id;
            UE4ServerData[] toRemove = Proc.Select(n => n).Where(n => n.Id == procId).ToArray();
            if (toRemove.Count() > 0)
            {
                Dispatcher.Invoke(() => { Proc.Remove(toRemove[0]); });
            }
        }

        private void DestroyServer(string serverName)
        {
            UE4ServerData[] toDestroy = Proc.Select(n => n).Where(n => n.Name == serverName).ToArray();
            Process toKill = Process.GetProcessById(toDestroy[0].Id);
            Proc.Remove(toDestroy[0]);
            toKill.Kill();

        }

        private void CloseSelectedServer_Click(object sender, RoutedEventArgs e)
        {

            UE4ServerData currentSelected = (DG1.SelectedItem) as UE4ServerData;
            if (currentSelected != null)
            {
                Process toKill = Process.GetProcessById(currentSelected.Id);
                Proc.Remove(currentSelected);
                toKill.Kill();
                SelectedId.Text = "Server Id";
            }

        }

        private void DG1_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if(DG1.SelectedIndex != -1)
                SelectedId.Text = ((DG1.SelectedItem) as UE4ServerData).Id.ToString();
        }

    }
}
